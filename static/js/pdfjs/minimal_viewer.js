
/*
adapted from http://vivin.net/pub/pdfjs/

*/
var pdf;
var pdfBase64;

PDFJS.disableWorker = true; //Not using web workers. CAUTION ERROR

function change_page(event){
    event.preventDefault()

    var url = $(event.currentTarget).closest('a').attr('href');

    $('#pdf-content').empty();

    $.getJSON(url,load_pdf);
}



function load_pdf(data){
    console.log(data)

    if (data.unrenderable == true) {
        alert('There was a problem rendering this file.');
    }

    else {
        $('#previous').attr('href',data.prev_page_url);
        $('#next').attr('href',data.next_page_url);
        pageNum = data.page;
        pdfData = base64ToUint8Array(data.data);
        PDFDOM(pdfData);
    };
};


function base64ToUint8Array(base64) {
    var raw = atob(base64); //This is a native function that decodes a base64-encoded string.
    var uint8Array = new Uint8Array(new ArrayBuffer(raw.length));
    for (var i = 0; i < raw.length; i++) {
        uint8Array[i] = raw.charCodeAt(i);
    }

    return uint8Array;
}

var PDFDOM = function PDFLoadWithSelect(pdfData) {

    pdf = PDFJS.getDocument(pdfData);

    pdf.data.getPage(pageNum).then(renderPage);

    $('span#page_count').html(pdf.data.numPages);
    $('span#page_num').html(pageNum);

    $('#next').show();
    $('#previous').show();

    if (pageNum == pdf.data.numPages){
        $('#next').hide();
    }

    if (pageNum == 1){
        $('#previous').hide();
    }

    function renderPage(page) {
        var mycanvas = $("<canvas id='pdf-canvas'></canvas>");
        //Set the canvas height and width to the height and width of the viewport
        var canvas = mycanvas.get(0);
            canvas.width =  $('#pdf-content').width();

        var context = canvas.getContext("2d");
        var viewport = page.getViewport(canvas.width / page.getViewport(1.0).width);
        canvas.height = viewport.height;

        //Append the canvas to the pdf container div

        var mypdfContainer = $("#pdf-content");
        mypdfContainer.css("height", canvas.height + "px").css("width", canvas.width + "px");
        mypdfContainer.append(mycanvas);

            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };

        page.render(renderContext);


    }

};

